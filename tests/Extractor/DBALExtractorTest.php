<?php

namespace Soong\DBAL\Tests\Extractor;

use Soong\Contracts\Data\RecordFactory;
use Soong\Contracts\Exception\ExtractorException;
use Soong\Data\BasicRecordFactory;
use Soong\DBAL\Extractor\DBALExtractor;
use Soong\Tests\Contracts\Extractor\CountableExtractorTestBase;
use Soong\Tests\Contracts\Extractor\TestFilter;
use Soong\DBAL\Tests\DBALTesting;

/**
 * Tests the \Soong\Extractor\DBAL class.
 */
class DBALExtractorTest extends CountableExtractorTestBase
{
    use DBALTesting;

    /**
     * @inheritdoc
     */
    protected function setUp() : void
    {
        parent::setUp();
        $this->extractorClass = '\\' . DBALExtractor::class;
        $this->dbSetup();
    }

    /**
     * Test data with db setup.
     *
     *   Each value is an array representing one data set:
     *     table_name: Name of the table to use for the test data.
     *     sql: Array of SQL statements to create and populate test tables.
     *     key_properties: Schema of table key properties as passed in
     *       extractor configuration.
     *     data: Array of data rows to store in the table. Values in each row
     *       are keyed by column name.
     */
    protected function data() : array
    {
        $data = [];
        $data['various column types'] = [
            'table_name' => 'soong_dbal_extractor_test_various_types',
            'sql' => [
                'CREATE TABLE soong_dbal_extractor_test_various_types (positive INTEGER NOT NULL, negative INTEGER NOT NULL, numeric VARCHAR(255) NOT NULL, zero INTEGER NOT NULL, emptystring VARCHAR(255) NOT NULL, float DOUBLE PRECISION NOT NULL, string VARCHAR(255) NOT NULL, nullfield VARCHAR(255) DEFAULT NULL, PRIMARY KEY(positive))',
                "INSERT INTO soong_dbal_extractor_test_various_types VALUES(11,-23,'563',0,'',1.2345,'a string',NULL)",
            ],
            'key_properties' => [
                'positive' => [
                    'type' => 'integer',
                ]
            ],
            'data' => [
                [
                    'positive' => 11,
                    'negative' => -23,
                    'numeric' => '563',
                    'zero' => 0,
                    'emptystring' => '',
                    'string' => 'a string',
                    'float' => 1.2345,
                    'nullfield' => null,
                ],
            ],
        ];
        $data['empty table'] = [
            'table_name' => 'soong_dbal_extractor_test_empty_table',
            'sql' => [
                'CREATE TABLE soong_dbal_extractor_test_empty_table (id INTEGER NOT NULL, PRIMARY KEY(id))',
            ],
            'key_properties' => [
                'id' => [
                    'type' => 'integer',
                ]
            ],
            'data' => [],
        ];
        $data['multiple rows'] = [
            'table_name' => 'soong_dbal_extractor_test_multiple_rows',
            'sql' => [
                'CREATE TABLE soong_dbal_extractor_test_multiple_rows (id INTEGER NOT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, PRIMARY KEY(id))',
                "INSERT INTO soong_dbal_extractor_test_multiple_rows VALUES(1,'A common name','This row comes first.')",
                "INSERT INTO soong_dbal_extractor_test_multiple_rows VALUES(2,'A different name','This row follows.')",
                "INSERT INTO soong_dbal_extractor_test_multiple_rows VALUES(3,'A common name','This row follows.')",
            ],
            'key_properties' => [
                'id' => [
                    'type' => 'integer',
                ]
            ],
            'data' => [
                0 => [
                    'id' => 1,
                    'name' => 'A common name',
                    'description' => 'This row comes first.',
                ],
                1 => [
                    'id' => 2,
                    'name' => 'A different name',
                    'description' => 'This row follows.',
                ],
                2 => [
                    'id' => 3,
                    'name' => 'A common name',
                    'description' => 'This row follows.',
                ],
            ],
        ];
        return $data;
    }

    /**
     * Test extraction of various types of values
     */
    public function extractAllDataProvider() : array
    {
        $dataProvided = [];
        foreach ($this->data() as $dataSetName => $dataSet) {
            $dataProvided[$dataSetName] = [
                [
                    // @todo Replace with mock.
                    'record_factory' => new BasicRecordFactory(),
                    'query' => 'SELECT * FROM ' . $dataSet['table_name'] .
                        ' ORDER BY ' . implode(',', array_keys($dataSet['key_properties']))
                        . ' ASC',
                    'key_properties' => $dataSet['key_properties'],
                ],
                $dataSet['data'],
            ];
        }
        return $dataProvided;
    }

    /**
     * Test filtered extraction of various types of values.
     */
    public function extractFilteredDataProvider() : array
    {
        $data = [];
        // Just use the multiple-row data for testing filtering.
        $multipleRowScenario = $this->extractAllDataProvider()['multiple rows'];
        $baseConfiguration = $multipleRowScenario[0];
        $baseData = $multipleRowScenario[1];

        $data['no filters'] = [$baseConfiguration, $baseData];
        $data['simple filter, no results'] = [
            $baseConfiguration + ['filters' => [
                new TestFilter(['criteria' => ['id', 4]]),
            ]],
            [],
        ];
        $data['simple filter, one results'] = [
            $baseConfiguration + ['filters' => [
                new TestFilter(['criteria' => ['id', 2]]),
            ]],
            [$baseData[1]],
        ];
        $data['simple filter, two results'] = [
            $baseConfiguration + ['filters' => [
                new TestFilter(['criteria' => ['name', 'A common name']]),
            ]],
            [$baseData[0], $baseData[2]],
        ];
        $data['multiple filters, one result'] = [
            $baseConfiguration + ['filters' => [
                new TestFilter(['criteria' => ['name', 'A common name']]),
                new TestFilter(['criteria' => ['description', 'This row follows.']]),
            ]],
            [$baseData[2]],
        ];

        return $data;
    }

    /**
     * @inheritdoc
     *
     * @dataProvider extractAllDataProvider
     */
    public function testExtractAll(array $configuration, array $expected)
    {
        parent::testExtractAll(array_merge($this->configuration, $configuration), $expected);
    }

    /**
     * @inheritdoc
     *
     * @dataProvider extractFilteredDataProvider
     */
    public function testExtractFiltered(array $configuration, array $expected)
    {
        parent::testExtractFiltered(array_merge($this->configuration, $configuration), $expected);
    }

    /**
     * @inheritdoc
     *
     * @dataProvider extractAllDataProvider
     */
    public function testCount(array $configuration, $expected)
    {
        parent::testCount(array_merge($this->configuration, $configuration), $expected);
    }

    /**
     * Test retrieval of property metadata.
     */
    public function propertyDataProvider() : array
    {
        $data = [];
        foreach ($this->extractAllDataProvider() as $dataSetName => $dataSet) {
            $data[$dataSetName] = [$dataSet[0], [], $dataSet[0]['key_properties']];
        }
        return $data;
    }

    /**
     * @inheritdoc
     *
     * @dataProvider propertyDataProvider
     */
    public function testGetProperties(array $configuration, array $expectedProperties, array $expectedKeyProperties)
    {
        parent::testGetProperties(
            array_merge($this->configuration, $configuration),
            $expectedProperties,
            $expectedKeyProperties
        );
    }

    /**
     * Test for connection failures when extracting.
     */
    public function testConnectionException()
    {
        $configuration = [
            'connection' => ['url' => 'blah://blah'],
            'query' => '',
            'record_factory' => $this->createMock(RecordFactory::class),
        ];
        /** @var \Soong\Contracts\Extractor\Extractor $extractor */
        $extractor = new $this->extractorClass($configuration);
        $this->expectException(ExtractorException::class);
        $this->expectExceptionMessage('Unable to connect to database.');
        foreach ($extractor->extractAll() as $record) {
        }
    }

    /**
     * Test for query failures when extracting.
     */
    public function testQueryException()
    {
        $configuration = $this->configuration + [
            'query' => 'SELECT * FROM not_an_actual_table',
            'record_factory' => $this->createMock(RecordFactory::class),
        ];
        /** @var \Soong\Contracts\Extractor\Extractor $extractor */
        $extractor = new $this->extractorClass($configuration);
        $this->expectException(ExtractorException::class);
        $this->expectExceptionMessage("Error executing query 'SELECT * FROM not_an_actual_table'.");
        foreach ($extractor->extractAll() as $record) {
        }
    }
}
