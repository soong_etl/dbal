<?php

namespace Soong\DBAL\Tests\Loader;

use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\FetchMode;
use Soong\Contracts\Data\Record;
use Soong\Contracts\Exception\LoaderException;
use Soong\Data\BasicRecordFactory;
use Soong\Data\BasicRecordPayload;
use Soong\DBAL\Loader\DBALLoader;
use Soong\Tests\Contracts\Loader\LoaderTestBase;
use Soong\DBAL\Tests\DBALTesting;

/**
 * Tests the \Soong\Loader\DBAL class.
 */
class DBALLoaderTest extends LoaderTestBase
{
    use DBALTesting;

    /**
     * Specify the class we're testing.
     */
    protected function setUp() : void
    {
        parent::setUp();
        $this->loaderClass = '\\' . DBALLoader::class;
        $this->dbSetup();
    }

    /**
     * Basic test data.
     */
    protected function data() : array
    {
        $data = [];
        $data['various column types'] = [
            'table_name' => 'soong_dbal_loader_test_various_types',
            'sql' => [
                'CREATE TABLE soong_dbal_loader_test_various_types (positive INTEGER NOT NULL, negative INTEGER NOT NULL, numeric VARCHAR(255) NOT NULL, zero INTEGER NOT NULL, emptystring VARCHAR(255) NOT NULL, float DOUBLE PRECISION NOT NULL, string VARCHAR(255) NOT NULL, nullfield VARCHAR(255) DEFAULT NULL, PRIMARY KEY(positive))',
            ],
            'key_properties' => [
                'positive' => [
                    'type' => 'integer',
                ]
            ],
            'data' => [
                [
                    'positive' => 11,
                    'negative' => -23,
                    'numeric' => '563',
                    'zero' => 0,
                    'emptystring' => '',
                    'string' => 'a string',
                    'float' => 1.2345,
                    'nullfield' => null,
                ],
            ],
        ];
        $data['multiple rows'] = [
            'table_name' => 'soong_dbal_loader_test_multiple_rows',
            'sql' => [
                'CREATE TABLE soong_dbal_loader_test_multiple_rows (id INTEGER NOT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, PRIMARY KEY(id))',
            ],
            'key_properties' => [
                'id' => [
                    'type' => 'integer',
                ]
            ],
            'data' => [
                [
                    'id' => 1,
                    'name' => 'First Row',
                    'description' => 'This row comes first.',
                ],
                [
                    'id' => 2,
                    'name' => 'Second Row',
                    'description' => 'This row comes second.',
                ],
                [
                    'id' => 3,
                    'name' => 'Third Row',
                    'description' => 'This row follows the second.',
                ],
            ],
        ];
        return $data;
    }

    /**
     * Test loading of various types of values
     */
    public function loadDataProvider() : array
    {
        $dataProvided = [];
        foreach ($this->data() as $dataSetName => $dataSet) {
            $dataProvided[$dataSetName] = [
                'configuration' => [
                    'table' => $dataSet['table_name'],
                    'key_properties' => $dataSet['key_properties'],
                ],
                $dataSet['data'],
            ];
        }
        return $dataProvided;
    }

    /**
     * Test load().
     *
     * @dataProvider loadDataProvider
     */
    public function testLoad(array $configuration, array $dataSet)
    {
        $configuration = array_merge($this->configuration, $configuration);
        $connection = DriverManager::getConnection($configuration['connection']);
        $queryBuilder = $connection->createQueryBuilder();
        /** @var \Soong\Contracts\Loader\Loader $loader */
        $loader = new $this->loaderClass($configuration);
        $loadedCount = 0;
        // @todo Mock this.
        $recordFactory = new BasicRecordFactory();
        foreach ($dataSet as $dataRow) {
            $record = $recordFactory->create($dataRow);
            $payload = new BasicRecordPayload($record, $record);
            $loader($payload);
            // @todo Test multi-column keys.
            $keyNames = array_keys($configuration['key_properties']);
            $keyName = reset($keyNames);
            $loadedData = $queryBuilder
                ->select(array_keys($dataRow))
                ->from($configuration['table'])
                ->where("$keyName = ?")
                ->setParameter(0, $dataRow[$keyName])
                ->execute()
                ->fetchAll(FetchMode::ASSOCIATIVE);
            $this->assertEquals($dataRow, reset($loadedData));
            $loadedCount++;
        }
        $this->assertEquals(count($dataSet), $loadedCount);
    }

    /**
     * Test retrieval of property metadata.
     */
    public function propertyDataProvider() : array
    {
        $data = [];
        foreach ($this->loadDataProvider() as $dataSetName => $dataSet) {
            $data[$dataSetName] = [
                $dataSet['configuration'] + ['connection' => []],
                [],
                $dataSet['configuration']['key_properties']];
        }
        return $data;
    }


    /**
     * Test for connection failures when loading.
     */
    public function testConnectionException()
    {
        $configuration = [
            'connection' => ['url' => 'blah://blah'],
            'table' => 'blah',
        ];
        /** @var \Soong\Contracts\Loader\Loader $loader */
        $loader = new $this->loaderClass($configuration);
        /** @var \Soong\Contracts\Data\Record $record */
        $record = $this->createMock(Record::class);
        $this->expectException(LoaderException::class);
        $this->expectExceptionMessage('Unable to connect to database.');
        // @todo Mock this
        $loader(new BasicRecordPayload($record, $record));
    }

    /**
     * Test for insert failures when inserting.
     */
    public function testQueryException()
    {
        $configuration = $this->configuration + [
            'table' => 'not_an_actual_table',
        ];
        /** @var \Soong\Contracts\Loader\Loader $loader */
        $loader = new $this->loaderClass($configuration);
        /** @var \Soong\Contracts\Data\Record $record */
        $record = $this->createMock(Record::class);
        $this->expectException(LoaderException::class);
        $this->expectExceptionMessage("Unable to insert into not_an_actual_table.");
        // @todo Mock this
        $loader(new BasicRecordPayload($record, $record));
    }
}
