<?php

namespace Soong\DBAL\Tests\KeyMap;

use Soong\Contracts\Exception\KeyMapException;
use Soong\DBAL\KeyMap\DBALKeyMap;
use Soong\Tests\Contracts\KeyMap\KeyMapTestBase;
use Soong\DBAL\Tests\DBALTesting;

/**
 * Tests the \Soong\KeyMap\DBAL class.
 */
class DBALKeyMapTest extends KeyMapTestBase
{
    use DBALTesting;

    /**
     * @inheritdoc
     */
    protected function setUp() : void
    {
        parent::setUp();
        $this->keyMapClass = '\\' . DBALKeyMap::class;
        $this->dbSetup();
        $this->configuration['table'] = 'test_key_map_table';
    }

    /**
     * Make sure we get a connection exception with a bad db URL.
     */
    public function testConnectionException() : void
    {
        $this->configuration['connection']['url'] = 'blah://blah';
        /** @var \Soong\Contracts\KeyMap\KeyMap $keyMap */
        $keyMap = new $this->keyMapClass($this->configuration);
        $this->expectException(KeyMapException::class);
        $this->expectExceptionMessage('Unable to connect to database.');
        $keyMap->saveKeyMap([1], [2]);
    }

    /**
     * Make sure we get an exception when table creation fails.
     */
    public function testCreateTableException() : void
    {
        $this->configuration['table'] = '';
        /** @var \Soong\Contracts\KeyMap\KeyMap $keyMap */
        $keyMap = new $this->keyMapClass($this->configuration);
        $this->expectException(KeyMapException::class);
        $this->expectExceptionMessage('Unable to create key map table.');
        $keyMap->saveKeyMap([1], [2]);
    }
}
