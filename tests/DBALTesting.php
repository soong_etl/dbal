<?php

namespace Soong\DBAL\Tests;

use Doctrine\DBAL\DriverManager;

trait DBALTesting
{

    /**
     * Filename of the sqlite database used for testing.
     *
     * @var string
     */
    protected $dbFile;

    /**
     * Configuration used in all test data sets.
     *
     * @var array
     */
    protected $configuration = [];

    protected function dbSetup() : void
    {
        // Populate the test data using a random db file.
        $this->dbFile = '/tmp/soong_test_' . bin2hex(random_bytes(6)) . '.sqlite';
        $this->configuration['connection'] = [
            'url' => 'sqlite:///' . $this->dbFile,
        ];
        if (!empty($data = $this->data())) {
            $connection = DriverManager::getConnection($this->configuration['connection']);
            foreach ($data as $dataSetInfo) {
                foreach ($dataSetInfo['sql'] as $sql) {
                    $connection->executeQuery($sql);
                }
            }
        }
    }

    /**
     * Dummy data() function for cases not requiring setup queries.
     */
    protected function data() : array
    {
        return [];
    }

    /**
     * inheritdoc
     */
    protected function tearDown() : void
    {
        // Remove the random db file for this test.
        @unlink($this->dbFile);
        parent::tearDown();
    }
}
